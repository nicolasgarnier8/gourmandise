<?php

require_once 'mod_' . $gestion . '/controleur/' . $gestion . 'Controleur.php';

if (isset($_REQUEST['action'])) {
    switch ($_REQUEST['action']) {
        case 'authentifier':
            authentifier($_REQUEST);
            break;
        case'deconnexion':
            deconnexion($_REQUEST);
            break;
        case 'form_ajouter':
            ajouter($_REQUEST);
            break;
        case 'ajouter':
            valAjouter($_REQUEST);
            break;
        case 'form_consulter':
            consulter($_REQUEST);
            break;
        case'form_modifier';
            modifier($_REQUEST);
            break;
        case'modifier';
            valModifier($_REQUEST);
            break;
        case'form_supprimer';
            supprimer($_REQUEST);
            break;
        case 'supprimer';
            valSupprimer($_REQUEST);
            break;
        default :
            echo'IMPOSSIBLE DE PASSER ICI !!!';
    }
} else {
    vueParDefaut($_REQUEST);
}