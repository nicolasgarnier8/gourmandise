<?php

function caVendeur($parametre) {
    $codeV = $_SESSION['login'];
    $cnx = getBd();
    $sql = "SELECT sum(total_ht)FROM commande,vendeur WHERE commande.code_v=vendeur.code_v AND login=?";
    $CA = executeRequete($cnx, $sql, array($codeV));
    return $CA;
}

function totalCA($parametre) {
    $cnx = getBd();
    $sql = "SELECT sum(total_ht) from commande";
    $caTT = executeRequete($cnx, $sql);
    return $caTT;
}

function totalClient($parametre) {
    $cnx = getBd();
    $sql = "SELECT COUNT(code_c) from client";
    $totalClient = executeRequete($cnx, $sql);
    return $totalClient;
}

function moyProdComm($parametre) {
    $codeV = $_SESSION['login'];
    $cnx = getBd();
    $sql = "SELECT round(avg(ligne_commande.quantite_demandee),2) FROM ligne_commande, commande,vendeur WHERE ligne_commande.numero=commande.numero AND commande.code_v=vendeur.code_v AND login=?";
    $idRequete = executeRequete($cnx, $sql, array($codeV));
    return $idRequete;
}
