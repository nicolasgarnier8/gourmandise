<?php

require_once 'mod_accueil/modele/accueilModele.php';

function vueParDefaut($parametre) {
    $CA = caVendeur($parametre);
    $caTT = totalCA($parametre);
    $totalClient = totalClient($parametre);
    $idRequete = moyProdComm($parametre);
    require_once 'mod_accueil/vue/accueilVue.php';
}
