

<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->

    <body>
        {include file='include/leftPanel.tpl'}
        {include file='include/rightPanel.tpl'}
        <div class="content mt-3">

            <div class="col-sm-12">
                <div class="alert  alert-success alert-dismissible fade show" role="alert">
                    <span class="badge badge-pill badge-success">Bonjour</span> {$smarty.session.prenom} {$smarty.session.nom} votre CA en 2018 est de {$CA}  €
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>

            <div>&nbsp;</div>

            <br>

            <div class="animated fadeIn">
                <div class="row">


                    <div class="col-xl-3 col-lg-6">

                        <div class="card">
                            <div class="card-body">
                                <div class="stat-widget-one">
                                    <div class="stat-icon dib"><i class="ti-layout-grid2 text-warning border-warning"></i></div>
                                    <div class="stat-content dib">
                                        <div class="stat-text">Chiffre d'affaires global pour l'année 2018</div>
                                        <div class="stat-digit">{$caTT} €</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="stat-widget-one">
                                    <div class="stat-icon dib"><i class="ti-layout-grid2 text-warning border-warning"></i></div>
                                    <div class="stat-content dib">
                                        <div class="stat-text">Chiffre d'affaires global pour l'année 2018</div>
                                        <div class="stat-digit">{$caTT} €</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="stat-widget-one">
                                    <div class="stat-icon dib"><i class="ti-user text-primary border-primary"></i></div>
                                    <div class="stat-content dib">
                                        <div class="stat-text">Nombre de Clients total en 2018</div>
                                        <div class="stat-digit">{$totalClient}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="stat-widget-one">
                                    <div class="stat-icon dib"><i class="ti-link text-danger border-danger"></i></div>
                                    <div class="stat-content dib">
                                        <div class="stat-text">Moyenne de produits par commande</div>
                                        <div class="stat-digit">{$idRequete} produits</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- /# column -->
                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="mb-3">Le chiffre d'affaires des 8 meilleurs clients</h4>

                                <div id="clientTab" style="visibility:hidden; height:2px;">["LEGROS Christian","GAUTON Nadine","TARINAUX Lucien","DUMOITIERS Lucille","COTOY Sylvie","RABIN Sandrine","HELLOU Bernard","BOUCHE Carole"]</div>
                                <div id="caTab" style="visibility:hidden; height:2px;">["33400.07","15207.90","5649.44","811.92","480.63","473.61","446.27","354.28"]</div>
                                <canvas id="barChart"></canvas>

                            </div>
                        </div>


                    </div><!-- /# column -->

                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-body">

                                <h4 class="mb-3">Les produits en vedette</h4>

                                <div id="designationTab" style="visibility:hidden; height:2px;">["CARACAO","FEU DE JOIE LIQUEUR ASSORT.","TENDRE FRUIT","BOULE DE GLACE NOIRE","ZAN ALESAN","COCOMALLOW","COKTAIL","CARAMEL AU LAIT"]</div>
                                <div id="qteTab" style="visibility:hidden; height:2px;">["1014","571","110","40","14","11","6","6"]</div>
                                <canvas id="doughutChart"></canvas>

                            </div>
                        </div>


                    </div><!-- /# column -->
                </div>

            </div> <!-- .content -->
        </div><!-- /#right-panel -->

        <!-- Right Panel -->

        <script src="template/assets/js/vendor/jquery-2.1.4.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"></script>
        <script src="template/assets/js/plugins.js"></script>
        <script src="template/assets/js/main.js"></script>


        <script src="template/assets/js/lib/chart-js/Chart.bundle.js"></script>
        <script src="template/assets/js/lib/chart-js/chartGourmandise.js"></script>

        <script src="template/assets/js/dashboard.js"></script>
        <script src="template/assets/js/widgets.js"></script>
        <script src="template/assets/js/lib/vector-map/jquery.vmap.js"></script>
        <script src="template/assets/js/lib/vector-map/jquery.vmap.min.js"></script>
        <script src="template/assets/js/lib/vector-map/jquery.vmap.sampledata.js"></script>
        <script src="template/assets/js/lib/vector-map/country/jquery.vmap.world.js"></script>





        <script>
            (function ($) {
                "use strict";

                jQuery('#vmap').vectorMap({
                    map: 'world_en',
                    backgroundColor: null,
                    color: '#ffffff',
                    hoverOpacity: 0.7,
                    selectedColor: '#1de9b6',
                    enableZoom: true,
                    showTooltip: true,
                    values: sample_data,
                    scaleColors: ['#1de9b6', '#03a9f5'],
                    normalizeFunction: 'polynomial'
                });
            })(jQuery);
        </script>



    </body>
</html>
