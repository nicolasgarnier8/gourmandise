<?php
/* Smarty version 3.1.33, created on 2019-04-15 14:19:24
  from 'C:\xampp\htdocs\gourmandise\mod_produit\vue\produitVueListe.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5cb476cc597e00_38850738',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '655735f2bb8fd6c60b7bfc5e2fe045bc6e4c042c' => 
    array (
      0 => 'C:\\xampp\\htdocs\\gourmandise\\mod_produit\\vue\\produitVueListe.tpl',
      1 => 1555330757,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:include/leftPanel.tpl' => 1,
    'file:include/rightPanel.tpl' => 1,
  ),
),false)) {
function content_5cb476cc597e00_38850738 (Smarty_Internal_Template $_smarty_tpl) {
?><!DOCTYPE html>

<html>
    <head>
        <title></title>
    </head>
    <body>
        <?php $_smarty_tpl->_subTemplateRender('file:include/leftPanel.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
        <?php $_smarty_tpl->_subTemplateRender('file:include/rightPanel.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>La gourmandise, ça se partage !</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="index.php">Accueil</a></li>
                            <li><a href="index.php?gestion=produit">Produits</a></li>
                            <li class="active">Liste des produits</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content mt-3">
            <div class="animated fadeIn">

                <div class="row">

                    <div class="col-md-12">

                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title">Liste des produits

                                    <form class="pos-ajout" method="POST" action="index.php">
                                        <input type="hidden" name="gestion" value="produit">
                                        <input type="hidden" name="action" value="form_ajouter">
                                        <label>Ajouter un Produit : <input id="aImage" type="image" name="btn_ajouter" src='template/images/icones/a16.png'></label>
                                    </form>
                                </strong>
                            </div>
                            <div class="card-body">
                                <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Référence</th>
                                            <th>Désignation</th>
                                            <th>Tarif HT</th>
                                            <th>Sock</th>
                                            <th class="pos-actions">Consulter</th>
                                            <th class="pos-actions">Modifier</th>
                                            <th class="pos-actions">Supprimer</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['listerProduits']->value, 'produit');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['produit']->value) {
?>
                                            <tr>
                                                <td><?php echo $_smarty_tpl->tpl_vars['produit']->value['reference'];?>
</td>
                                                <td><?php echo $_smarty_tpl->tpl_vars['produit']->value['designation'];?>
</td>
                                                <td><?php echo $_smarty_tpl->tpl_vars['produit']->value['prix_unitaire_HT'];?>
</td>
                                                <td><?php echo $_smarty_tpl->tpl_vars['produit']->value['stock'];?>
</td>
                                                <td class="pos-actions">
                                                    <form method="POST" action="index.php">
                                                        <input type="hidden" name="gestion" value="produit">
                                                        <input type="hidden" name="action" value="form_consulter">
                                                        <input type="hidden" name="f_reference" value="<?php echo $_smarty_tpl->tpl_vars['produit']->value['reference'];?>
">
                                                        <input id="pImage" type="image" name="btn_consulter" src='template/images/icones/p16.png'>
                                                    </form
                                                </td>
                                                <td class="pos-actions">
                                                    <form method="POST" action="index.php">
                                                        <input type="hidden" name="gestion" value="produit">
                                                        <input type="hidden" name="action" value="form_modifier">
                                                        <input type="hidden" name="f_reference" value="<?php echo $_smarty_tpl->tpl_vars['produit']->value['reference'];?>
">
                                                        <input id="mImage" type="image" name="btn_modifier" src='template/images/icones/m16.png'>
                                                    </form>
                                                </td>
                                                <td class="pos-actions">
                                                    <form method="POST" action="index.php">
                                                        <input type="hidden" name="gestion" value="produit">
                                                        <input type="hidden" name="action" value="form_supprimer">
                                                        <input type="hidden" name="f_reference" value="<?php echo $_smarty_tpl->tpl_vars['produit']->value['reference'];?>
">
                                                        <input id="sImage" type="image" name="btn_supprimer" src='template/images/icones/s16.png'>
                                                    </form>
                                                </td>
                                            </tr>
                                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
<?php }
}
