<?php
/* Smarty version 3.1.33, created on 2019-04-18 11:34:38
  from 'C:\xampp\htdocs\gourmandise\mod_client\vue\clientVueFiche.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5cb844ae25e825_86858037',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'db6d06aebfc70c47901db0b5d98ae7d821dda22e' => 
    array (
      0 => 'C:\\xampp\\htdocs\\gourmandise\\mod_client\\vue\\clientVueFiche.tpl',
      1 => 1555579961,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:include/leftPanel.tpl' => 1,
    'file:include/rightPanel.tpl' => 1,
  ),
),false)) {
function content_5cb844ae25e825_86858037 (Smarty_Internal_Template $_smarty_tpl) {
?>

<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->

    <body>
        <?php $_smarty_tpl->_subTemplateRender('file:include/leftPanel.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
        <?php $_smarty_tpl->_subTemplateRender('file:include/rightPanel.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>La gourmandise, ça se partage !</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="index.php">Accueil</a></li>
                            <li><a href="index.php?gestion=client">Clients</a></li>
                            <li class="active"><?php echo $_smarty_tpl->tpl_vars['msg']->value;?>
</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content mt-3">
            <div class="animated fadeIn">

                <div class="row">

                    <div class="col-md-6">

                        <div class="card">
                            <div class="card-header"><strong><?php echo $_smarty_tpl->tpl_vars['msg']->value;?>
</strong></div>
                            <form action="index.php" method="POST">
                                <input type="hidden" name="gestion"  value="client" >
                                <input type="hidden" name="action"  value=<?php echo $_smarty_tpl->tpl_vars['action']->value;?>
 >
                                <div class="card-body card-block">
                                    <input type="hidden" name="f_code_c"  class="form-control" readonly="readonly" value=<?php echo $_smarty_tpl->tpl_vars['code_c']->value;?>
>
                                    <div class="form-group"><label for="nom" class=" form-control-label">Nom et Prénom : </label><input type="text" name="f_nom" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['nom']->value;?>
"></div>
                                    <div class="form-group"><label for="adresse" class=" form-control-label">Adresse : </label><input type="text" name="f_adresse" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['adresse']->value;?>
"></div>
                                    <div class="form-group"><label for="cp" class=" form-control-label">Code Postal : </label><input type="text" name="f_cp" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['cp']->value;?>
"></div>
                                    <div class="form-group"><label for="ville" class=" form-control-label">Ville : </label><input type="text" name="f_ville" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['ville']->value;?>
"></div>
                                    <div class="form-group"><label for="telephone" class=" form-control-label">Téléphone : </label><input type="text" name="f_telephone" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['telephone']->value;?>
"></div>

                                </div>
                                <div class="card-body card-block">
                                    <div class="col-md-6"> <input type='button' class="btn btn-submit" value='Retour' onclick='location.href = "index.php?gestion=client"'></div>

                                    <?php if ($_smarty_tpl->tpl_vars['action']->value == 'ajouter') {?> <div class="col-md-6 "> <input type="submit" id="f_btn-action" class="btn btn-submit pos-btn-action" value="Ajouter" ></div><?php }?>
                                    <?php if ($_smarty_tpl->tpl_vars['action']->value == 'modifier') {?> <div class="col-md-6 "> <input type="submit" id="f_btn-action" class="btn btn-submit pos-btn-action" value="Modifier" ></div><?php }?>
                                    <?php if ($_smarty_tpl->tpl_vars['action']->value == 'supprimer') {?> <div class="col-md-6 "> <input type="submit" id="f_btn-action" class="btn btn-submit pos-btn-action" value="Supprimer" ></div><?php }?>
                                    <br>
                                </div>
                            </form>
                        </div>
                    </div>
                </div><!-- .animated -->
            </div><!-- .content -->


        </div><!-- /#right-panel -->

        <!-- Right Panel -->
        <?php echo '<script'; ?>
 src="template/assets/js/vendor/jquery-2.1.4.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/assets/js/plugins.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/assets/js/main.js"><?php echo '</script'; ?>
>


        <?php echo '<script'; ?>
 src="template/assets/js/lib/data-table/datatables.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/assets/js/lib/data-table/dataTables.bootstrap.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/assets/js/lib/data-table/dataTables.buttons.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/assets/js/lib/data-table/buttons.bootstrap.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/assets/js/lib/data-table/jszip.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/assets/js/lib/data-table/pdfmake.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/assets/js/lib/data-table/vfs_fonts.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/assets/js/lib/data-table/buttons.html5.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/assets/js/lib/data-table/buttons.print.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/assets/js/lib/data-table/buttons.colVis.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/assets/js/lib/data-table/datatables-init.js"><?php echo '</script'; ?>
>


        <?php echo '<script'; ?>
 type="text/javascript">
                                        $(document).ready(function () {
                                            $('#bootstrap-data-table-export').DataTable();
                                        });
        <?php echo '</script'; ?>
>

    </body>
</html>
<?php }
}
