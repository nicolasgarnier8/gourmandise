<?php
/* Smarty version 3.1.33, created on 2019-04-19 10:09:00
  from 'C:\xampp\htdocs\gourmandise\mod_commande\vue\commandeVueFiche.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5cb9821c048268_32961824',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '8259b0be09e012193550501b9a8f24b73fc6c344' => 
    array (
      0 => 'C:\\xampp\\htdocs\\gourmandise\\mod_commande\\vue\\commandeVueFiche.tpl',
      1 => 1555661329,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:include/leftPanel.tpl' => 1,
    'file:include/rightPanel.tpl' => 1,
  ),
),false)) {
function content_5cb9821c048268_32961824 (Smarty_Internal_Template $_smarty_tpl) {
?>

<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
    <body>
        <?php $_smarty_tpl->_subTemplateRender('file:include/leftPanel.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
        <?php $_smarty_tpl->_subTemplateRender('file:include/rightPanel.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>La gourmandise, ça se partage !</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="index.php">Accueil</a></li>
                            <li><a href="index.php?gestion=commande">Commandes</a></li>
                            <li class="active">Fiche Commande : Consultation</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content mt-3">
            <div class="animated fadeIn">

                <div class="row">

                    <form action="index.php" method="POST">
                        <input type="hidden" name="gestion"  value="commande" >
                        <input type="hidden" name="action"  value="" >
                        <div class="col-md-12">

                            <div class="col-md-6">
                                <div class="card">
                                    <div class="card-header"><strong>Fiche Commande : Consultation</strong></div>

                                    <div class="card-body card-block" >

                                        <div class="form-group"name="f_numero">Numero : <?php echo $_smarty_tpl->tpl_vars['numero']->value;?>
</div>
                                        <div class="form-group">Vendeur : <?php echo $_smarty_tpl->tpl_vars['nom']->value;?>
 <?php echo $_smarty_tpl->tpl_vars['prenom']->value;?>
</div>
                                        <div class="form-group">Code Client : <?php echo $_smarty_tpl->tpl_vars['code_c']->value;?>
 </div>
                                        <div class="form-group">Client : <?php echo $_smarty_tpl->tpl_vars['nom_c']->value;?>
 </div>


                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">

                                <div class="card">
                                    <div class="card-header"><strong>Etat de la commande</strong></div>
                                    <div class="card-body card-block">

                                        <div class="form-group">Date de commande : <?php echo $_smarty_tpl->tpl_vars['date_commande']->value;?>
</div>
                                        <div class="form-group">Date de livraison : <?php echo $_smarty_tpl->tpl_vars['date_livraison']->value;?>
</div>
                                        <div class="form-group">Total HT : <?php echo $_smarty_tpl->tpl_vars['total_ht']->value;?>
 € </div>
                                        <div class="form-group">Commande Validée : <?php echo $_smarty_tpl->tpl_vars['etat']->value;?>
 </div>


                                    </div>
                                </div>
                            </div>



                        </div>

                        <div class="col-md-12">
                            <!-- Liste lignes de commande -->

                            <div class="card-body">
                                <table  class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>N° Ligne</th>
                                            <th>Référence</th>
                                            <th>Désignation</th>
                                            <th>Quantité</th>
                                            <th>prix</th>
                                            <!--<th class="pos-actions">Consulter</th>
                                            <th class="pos-actions">Modifier</th>
                                            <th class="pos-actions">Supprimer</th>-->

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['listeCommande']->value, 'ligne_commande');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['ligne_commande']->value) {
?>
                                            <tr>
                                                <td><?php echo $_smarty_tpl->tpl_vars['ligne_commande']->value['numero_ligne'];?>
</td>
                                                <td><?php echo $_smarty_tpl->tpl_vars['ligne_commande']->value['reference'];?>
</td>
                                                <td><?php echo $_smarty_tpl->tpl_vars['ligne_commande']->value['designation'];?>
</td>
                                                <td><?php echo $_smarty_tpl->tpl_vars['ligne_commande']->value['quantite_demandee'];?>
</td>
                                                <td><?php echo $_smarty_tpl->tpl_vars['ligne_commande']->value['prix_unitaire_HT']*1.357;?>
</td>

                                            </tr>
                                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                        <tr>
                                            <td colspan="3"> Montant de la commande : 226.62 € </td>
                                            <td colspan="2"> Total TVA : 45.32 €</td>
                                        </tr>

                                    </tbody>
                                </table>


                            </div>
                            <div class="card-body card-block">
                                <div class="col-md-6"> <input type='button' class="btn btn-submit" value='Retour' onclick='location.href = "index.php?gestion=commande"'></div>
                                <div class="col-md-6 "> </div>
                                <br>
                            </div>

                        </div>
                    </form>

                </div><!-- .content -->


            </div><!-- /#right-panel -->

            <!-- Right Panel -->
            <?php echo '<script'; ?>
 src="template/assets/js/vendor/jquery-2.1.4.min.js"><?php echo '</script'; ?>
>
            <?php echo '<script'; ?>
 src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"><?php echo '</script'; ?>
>
            <?php echo '<script'; ?>
 src="template/assets/js/plugins.js"><?php echo '</script'; ?>
>
            <?php echo '<script'; ?>
 src="template/assets/js/main.js"><?php echo '</script'; ?>
>


            <?php echo '<script'; ?>
 src="template/assets/js/lib/data-table/datatables.min.js"><?php echo '</script'; ?>
>
            <?php echo '<script'; ?>
 src="template/assets/js/lib/data-table/dataTables.bootstrap.min.js"><?php echo '</script'; ?>
>
            <?php echo '<script'; ?>
 src="template/assets/js/lib/data-table/dataTables.buttons.min.js"><?php echo '</script'; ?>
>
            <?php echo '<script'; ?>
 src="template/assets/js/lib/data-table/buttons.bootstrap.min.js"><?php echo '</script'; ?>
>
            <?php echo '<script'; ?>
 src="template/assets/js/lib/data-table/jszip.min.js"><?php echo '</script'; ?>
>
            <?php echo '<script'; ?>
 src="template/assets/js/lib/data-table/pdfmake.min.js"><?php echo '</script'; ?>
>
            <?php echo '<script'; ?>
 src="template/assets/js/lib/data-table/vfs_fonts.js"><?php echo '</script'; ?>
>
            <?php echo '<script'; ?>
 src="template/assets/js/lib/data-table/buttons.html5.min.js"><?php echo '</script'; ?>
>
            <?php echo '<script'; ?>
 src="template/assets/js/lib/data-table/buttons.print.min.js"><?php echo '</script'; ?>
>
            <?php echo '<script'; ?>
 src="template/assets/js/lib/data-table/buttons.colVis.min.js"><?php echo '</script'; ?>
>
            <?php echo '<script'; ?>
 src="template/assets/js/lib/data-table/datatables-init.js"><?php echo '</script'; ?>
>


            <?php echo '<script'; ?>
 type="text/javascript">
                                    $(document).ready(function () {
                                        $('#bootstrap-data-table-export').DataTable();
                                    });
            <?php echo '</script'; ?>
>

    </body>
</html>
<?php }
}
