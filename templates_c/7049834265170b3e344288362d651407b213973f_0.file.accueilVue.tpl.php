<?php
/* Smarty version 3.1.33, created on 2019-04-18 09:22:54
  from 'C:\xampp\htdocs\gourmandise\mod_accueil\vue\accueilVue.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5cb825ce124747_09194873',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '7049834265170b3e344288362d651407b213973f' => 
    array (
      0 => 'C:\\xampp\\htdocs\\gourmandise\\mod_accueil\\vue\\accueilVue.tpl',
      1 => 1555572171,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:include/leftPanel.tpl' => 1,
    'file:include/rightPanel.tpl' => 1,
  ),
),false)) {
function content_5cb825ce124747_09194873 (Smarty_Internal_Template $_smarty_tpl) {
?>

<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->

    <body>
        <?php $_smarty_tpl->_subTemplateRender('file:include/leftPanel.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
        <?php $_smarty_tpl->_subTemplateRender('file:include/rightPanel.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
        <div class="content mt-3">

            <div class="col-sm-12">
                <div class="alert  alert-success alert-dismissible fade show" role="alert">
                    <span class="badge badge-pill badge-success">Bonjour</span> <?php echo $_SESSION['prenom'];?>
 <?php echo $_SESSION['nom'];?>
 votre CA en 2018 est de <?php echo $_smarty_tpl->tpl_vars['CA']->value;?>
  €
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>

            <div>&nbsp;</div>

            <br>

            <div class="animated fadeIn">
                <div class="row">


                    <div class="col-xl-3 col-lg-6">

                        <div class="card">
                            <div class="card-body">
                                <div class="stat-widget-one">
                                    <div class="stat-icon dib"><i class="ti-layout-grid2 text-warning border-warning"></i></div>
                                    <div class="stat-content dib">
                                        <div class="stat-text">Chiffre d'affaires global pour l'année 2018</div>
                                        <div class="stat-digit"><?php echo $_smarty_tpl->tpl_vars['caTT']->value;?>
 €</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="stat-widget-one">
                                    <div class="stat-icon dib"><i class="ti-layout-grid2 text-warning border-warning"></i></div>
                                    <div class="stat-content dib">
                                        <div class="stat-text">Chiffre d'affaires global pour l'année 2018</div>
                                        <div class="stat-digit"><?php echo $_smarty_tpl->tpl_vars['caTT']->value;?>
 €</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="stat-widget-one">
                                    <div class="stat-icon dib"><i class="ti-user text-primary border-primary"></i></div>
                                    <div class="stat-content dib">
                                        <div class="stat-text">Nombre de Clients total en 2018</div>
                                        <div class="stat-digit"><?php echo $_smarty_tpl->tpl_vars['totalClient']->value;?>
</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="stat-widget-one">
                                    <div class="stat-icon dib"><i class="ti-link text-danger border-danger"></i></div>
                                    <div class="stat-content dib">
                                        <div class="stat-text">Moyenne de produits par commande</div>
                                        <div class="stat-digit"><?php echo $_smarty_tpl->tpl_vars['idRequete']->value;?>
 produits</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- /# column -->
                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="mb-3">Le chiffre d'affaires des 8 meilleurs clients</h4>

                                <div id="clientTab" style="visibility:hidden; height:2px;">["LEGROS Christian","GAUTON Nadine","TARINAUX Lucien","DUMOITIERS Lucille","COTOY Sylvie","RABIN Sandrine","HELLOU Bernard","BOUCHE Carole"]</div>
                                <div id="caTab" style="visibility:hidden; height:2px;">["33400.07","15207.90","5649.44","811.92","480.63","473.61","446.27","354.28"]</div>
                                <canvas id="barChart"></canvas>

                            </div>
                        </div>


                    </div><!-- /# column -->

                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-body">

                                <h4 class="mb-3">Les produits en vedette</h4>

                                <div id="designationTab" style="visibility:hidden; height:2px;">["CARACAO","FEU DE JOIE LIQUEUR ASSORT.","TENDRE FRUIT","BOULE DE GLACE NOIRE","ZAN ALESAN","COCOMALLOW","COKTAIL","CARAMEL AU LAIT"]</div>
                                <div id="qteTab" style="visibility:hidden; height:2px;">["1014","571","110","40","14","11","6","6"]</div>
                                <canvas id="doughutChart"></canvas>

                            </div>
                        </div>


                    </div><!-- /# column -->
                </div>

            </div> <!-- .content -->
        </div><!-- /#right-panel -->

        <!-- Right Panel -->

        <?php echo '<script'; ?>
 src="template/assets/js/vendor/jquery-2.1.4.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/assets/js/plugins.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/assets/js/main.js"><?php echo '</script'; ?>
>


        <?php echo '<script'; ?>
 src="template/assets/js/lib/chart-js/Chart.bundle.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/assets/js/lib/chart-js/chartGourmandise.js"><?php echo '</script'; ?>
>

        <?php echo '<script'; ?>
 src="template/assets/js/dashboard.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/assets/js/widgets.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/assets/js/lib/vector-map/jquery.vmap.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/assets/js/lib/vector-map/jquery.vmap.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/assets/js/lib/vector-map/jquery.vmap.sampledata.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/assets/js/lib/vector-map/country/jquery.vmap.world.js"><?php echo '</script'; ?>
>





        <?php echo '<script'; ?>
>
            (function ($) {
                "use strict";

                jQuery('#vmap').vectorMap({
                    map: 'world_en',
                    backgroundColor: null,
                    color: '#ffffff',
                    hoverOpacity: 0.7,
                    selectedColor: '#1de9b6',
                    enableZoom: true,
                    showTooltip: true,
                    values: sample_data,
                    scaleColors: ['#1de9b6', '#03a9f5'],
                    normalizeFunction: 'polynomial'
                });
            })(jQuery);
        <?php echo '</script'; ?>
>



    </body>
</html>
<?php }
}
