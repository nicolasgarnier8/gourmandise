<?php
/* Smarty version 3.1.33, created on 2019-04-18 16:20:16
  from 'C:\xampp\htdocs\gourmandise\mod_commande\vue\commandeVueListe.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5cb887a032c6d6_23525172',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '61209324b6bf6d63725e9c0bb6dd76e6f676ba63' => 
    array (
      0 => 'C:\\xampp\\htdocs\\gourmandise\\mod_commande\\vue\\commandeVueListe.tpl',
      1 => 1555597212,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:include/leftPanel.tpl' => 1,
    'file:include/rightPanel.tpl' => 1,
  ),
),false)) {
function content_5cb887a032c6d6_23525172 (Smarty_Internal_Template $_smarty_tpl) {
?>

<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
    <body>

        <?php $_smarty_tpl->_subTemplateRender('file:include/leftPanel.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
        <?php $_smarty_tpl->_subTemplateRender('file:include/rightPanel.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>La gourmandise, ça se partage !</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="index.php">Accueil</a></li>
                            <li><a href="index.php?gestion=commande">Commandes</a></li>
                            <li class="active">Liste des commandes</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content mt-3">
            <div class="animated fadeIn">

                <div class="row">

                    <div class="col-md-12">

                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title">Liste des commandes

                                    <form class="pos-ajout" method="POST" action="index.php">
                                        <input type="hidden" name="gestion" value="commande">
                                        <input type="hidden" name="action" value="form_ajouter">
                                        <label>Passer une commande : <input id="aImage" type="image" name="btn_ajouter" src='template/images/icones/a16.png'></label>
                                    </form>
                                </strong>
                            </div>
                            <div class="card-body">
                                <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Numéro</th>
                                            <th>Vendeur</th>
                                            <th>Client</th>
                                            <th>Montant HT</th>
                                            <th class="pos-actions">Consulter</th>
                                            <th class="pos-actions">Modifier</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['listeCommande']->value, 'commande');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['commande']->value) {
?>
                                            <tr>
                                                <td><?php echo $_smarty_tpl->tpl_vars['commande']->value['numero'];?>
</td>
                                                <td><?php echo $_smarty_tpl->tpl_vars['commande']->value['nom'];?>
 <?php echo $_smarty_tpl->tpl_vars['commande']->value['prenom'];?>
</td>
                                                <td><a href="index.php?gestion=client&action=form_consulter&pop=1&f_code_c=47" target="_blank"><?php echo $_smarty_tpl->tpl_vars['commande']->value['code_c'];?>
 <?php echo $_smarty_tpl->tpl_vars['commande']->value['nom_c'];?>
</a></td>
                                                <td><?php echo $_smarty_tpl->tpl_vars['commande']->value['total_ht'];?>
</td>
                                                <td class="pos-actions">
                                                    <form method="POST" action="index.php">
                                                        <input type="hidden" name="gestion" value="commande">
                                                        <input type="hidden" name="action" value="form_consulter">
                                                        <input type="hidden" name="f_numero" value="<?php echo $_smarty_tpl->tpl_vars['commande']->value['numero'];?>
">
                                                        <input id="pImage" type="image" name="btn_consulter" src='template/images/icones/p16.png'>
                                                    </form
                                                </td>
                                                <td class="pos-actions">
                                                    Validée
                                                </td>
                                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>


                </div><!-- .animated -->
            </div><!-- .content -->


        </div><!-- /#right-panel -->

        <!-- Right Panel -->
        <?php echo '<script'; ?>
 src="template/assets/js/vendor/jquery-2.1.4.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/assets/js/plugins.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/assets/js/main.js"><?php echo '</script'; ?>
>


        <?php echo '<script'; ?>
 src="template/assets/js/lib/data-table/datatables.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/assets/js/lib/data-table/dataTables.bootstrap.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/assets/js/lib/data-table/dataTables.buttons.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/assets/js/lib/data-table/buttons.bootstrap.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/assets/js/lib/data-table/jszip.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/assets/js/lib/data-table/pdfmake.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/assets/js/lib/data-table/vfs_fonts.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/assets/js/lib/data-table/buttons.html5.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/assets/js/lib/data-table/buttons.print.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/assets/js/lib/data-table/buttons.colVis.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/assets/js/lib/data-table/datatables-init.js"><?php echo '</script'; ?>
>


        <?php echo '<script'; ?>
 type="text/javascript">
            $(document).ready(function () {
                $('#bootstrap-data-table-export').DataTable();
            });
        <?php echo '</script'; ?>
>

    </body>
</html>
<?php }
}
