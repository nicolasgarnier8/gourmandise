<?php
/* Smarty version 3.1.33, created on 2019-04-18 11:21:00
  from 'C:\xampp\htdocs\gourmandise\mod_profil\vue\profilVue.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5cb8417c1e54d2_56128846',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '8da1e36a9de98e0fda35baa578dbfc11600d7420' => 
    array (
      0 => 'C:\\xampp\\htdocs\\gourmandise\\mod_profil\\vue\\profilVue.tpl',
      1 => 1555579249,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:include/leftPanel.tpl' => 1,
    'file:include/rightPanel.tpl' => 1,
  ),
),false)) {
function content_5cb8417c1e54d2_56128846 (Smarty_Internal_Template $_smarty_tpl) {
?>

<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>

    </head>
    <body>
        <?php $_smarty_tpl->_subTemplateRender('file:include/leftPanel.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
        <?php $_smarty_tpl->_subTemplateRender('file:include/rightPanel.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>La gourmandise, ça se partage !</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="index.php">Accueil</a></li>
                            <li><a href="index.php?gestion=profil&action=form_modifier">Mon profil</a></li>
                            <li class="active">Mon Profil</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content mt-3">
            <div class="animated fadeIn">

                <div class="row">

                    <div class="col-md-6">

                        <div class="card">
                            <div class="card-header"><strong>Mon Profil</strong></div>
                            <form action="index.php" method="POST">
                                <input type="hidden" name="gestion"  value="profil" >
                                <input type="hidden" name="action"  value="modifier" >
                                <div class="card-body card-block">

                                    <div class="form-group"><label for="code_v" class=" form-control-label">Code Vendeur : </label><input type="text" name="f_code_v"  class="form-control" readonly="readonly" value="<?php echo $_SESSION['code_v'];?>
"></div>

                                    <div class="form-group"><label for="nom" class=" form-control-label">Nom : </label><input type="text" name="f_nom" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['nom']->value;?>
"></div>
                                    <div class="form-group"><label for="prenom" class=" form-control-label">Prénom :<br></label><input type="text" name="f_prenom" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['prenom']->value;?>
"></div>
                                    <div class="form-group"><label for="adresse" class=" form-control-label">Adresse :<br></label><input type="text" name="f_adresse" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['adresse']->value;?>
"></div>
                                    <div class="form-group"><label for="cp" class=" form-control-label">Code postal  : </label><input type="text" name="f_cp" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['cp']->value;?>
"></div>
                                    <div class="form-group"><label for="ville" class=" form-control-label">Ville  : </label><input type="text" name="f_ville" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['ville']->value;?>
"></div>
                                    <div class="form-group"><label for="telephone" class=" form-control-label">Téléphone : <br></label><input type="text" name="f_telephone" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['telephone']->value;?>
"></div>
                                    <div class="form-group"><label for="pw" class=" form-control-label">Mot de passe : <br></label><input type="password" name="f_pw"  id="f_pw" autocomplete="off" class="form-control" value="" ></div>
                                    <div class="form-group"><label for="confirmation" class=" form-control-label">Confirmation : <br></label><input  type="password" name="f_confirmation" id="f_confirmation" autocomplete="off"  oninput="verifPW()" class="form-control" value="" ></div>

                                </div>
                                <div class="card-body card-block">
                                    <div class="col-md-6"> <input type='button' class="btn btn-submit" value='Retour' onclick='location.href = "index.php"'></div>
                                    <div class="col-md-6 "> <input type="submit" id="f_btn-action" class="btn btn-submit pos-btn-action" value="Modifier" ></div>
                                    <br>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header"> <strong>Statistiques et informations</strong></div>
                            <div class="card-body card-block">
                                <div class="form-group"><strong>Montant total de mes ventes :</strong><br><?php echo $_smarty_tpl->tpl_vars['totalCA']->value;?>
 €</div>

                            </div>
                        </div>
                    </div>


                </div><!-- .animated -->
            </div><!-- .content -->


        </div><!-- /#right-panel -->

        <!-- Right Panel -->
        <?php echo '<script'; ?>
 src="template/assets/js/vendor/jquery-2.1.4.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/assets/js/plugins.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/assets/js/main.js"><?php echo '</script'; ?>
>


        <?php echo '<script'; ?>
 src="template/assets/js/lib/data-table/datatables.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/assets/js/lib/data-table/dataTables.bootstrap.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/assets/js/lib/data-table/dataTables.buttons.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/assets/js/lib/data-table/buttons.bootstrap.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/assets/js/lib/data-table/jszip.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/assets/js/lib/data-table/pdfmake.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/assets/js/lib/data-table/vfs_fonts.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/assets/js/lib/data-table/buttons.html5.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/assets/js/lib/data-table/buttons.print.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/assets/js/lib/data-table/buttons.colVis.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="template/assets/js/lib/data-table/datatables-init.js"><?php echo '</script'; ?>
>


        <?php echo '<script'; ?>
 type="text/javascript">
                                        $(document).ready(function () {
                                            $('#bootstrap-data-table-export').DataTable();
                                        });
        <?php echo '</script'; ?>
>

        <?php echo '<script'; ?>
>
            function verifPW() {

                var confirmation = document.getElementById('f_confirmation');

                if (document.getElementById('f_pw').value != confirmation.value) {

                    return confirmation.setCustomValidity('Les deux mots de passe doivent être identiques');
                }

                confirmation.setCustomValidity('');
            }
        <?php echo '</script'; ?>
>

    </body>
</html>
<?php }
}
