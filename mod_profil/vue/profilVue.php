<?php

$tpl = new Smarty();


if ($parametre['action'] == 'form_modifier') {
    $row = $vendeur->fetch();

    $tpl->assign('code_v', $row['code_v']);
    $tpl->assign('nom', $row['nom']);
    $tpl->assign('prenom', $row['prenom']);
    $tpl->assign('adresse', $row['adresse']);
    $tpl->assign('cp', $row['cp']);
    $tpl->assign('ville', $row['ville']);
    $tpl->assign('telephone', $row['telephone']);
}

$row = $totalCA->fetch();
$tpl->assign('totalCA', $row[0]);

$tpl->assign('action', $parametre['action']);
$tpl->display('mod_profil/vue/profilVue.tpl');
