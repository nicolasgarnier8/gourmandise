<?php

function profilVendeur($parametre) {
    $codeV = $_SESSION['login'];
    $cnx = getBd();
    $sql = "SELECT * FROM vendeur WHERE login=?";
    $vendeur = executeRequete($cnx, $sql, array($codeV));
    return $vendeur;
}

function caTotal($parametre) {
    $codeV = $_SESSION['login'];
    $cnx = getBd();
    $sql = "SELECT sum(total_ht)FROM commande,vendeur WHERE commande.code_v=vendeur.code_v AND login=?";
    $totalCA = executeRequete($cnx, $sql, array($codeV));
    return $totalCA;
}
