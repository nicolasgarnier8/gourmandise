<?php

function executeRequete($cnx, $sql, $parametre = null) {
    if ($parametre == null) {
        $resultat = $cnx->query($sql);
    } else {
        $resultat = $cnx->prepare($sql);
        $resultat->execute($parametre);
    }
    return $resultat;
}

function getBd() {
    $cnx = connexion();
    return $cnx;
}

function connexion() {
    try {
        $cnx = new PDO('mysql:host=' . SRV . ';dbname=' . BD, USER, PW, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8", PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
        return $cnx;
    } catch (PDOException $recuperation) {
        echo 'erreur lors de la connexion :' . $recuperation->getMessage();
        exit;
    }
    return $cnx;
}
