<?php

require_once('mod_client/modele/clientModele.php');

function vueParDefaut($parametre) {
    $idRequete = listerClients();
    require_once 'mod_client/vue/clientVueListe.php';
}

function ajouter($parametre) {
    $msg = "Fiche Client : Création";
    require_once 'mod_client/vue/clientVueFiche.php';
}

function valAjouter($parametre) {
    $idRequete = ajouterClient($parametre);
    $idRequete = listerClients();
    require_once 'mod_client/vue/clientVueListe.php';
}

function consulter($parametre) {
    $msg = "Fiche Client : Consultation";
    $idRequete = consulterClient($parametre);
    require_once 'mod_client/vue/clientVueFiche.php';
}

function modifier($parametre) {
    $msg = "Fiche Client : Modification";
    $idRequete = consulterClient($parametre);
    require_once 'mod_client/vue/clientVueFiche.php';
}

function valModifier($parametre) {
    $idRequete = modifierClient($parametre);
    $idRequete = listerClients();
    require_once 'mod_client/vue/clientVueListe.php';
}

function supprimer($parametre) {
    $msg = "Fiche Client : Suppresion";
    $idRequete = consulterClient($parametre);
    require_once 'mod_client/vue/clientVueFiche.php';
}

function valSupprimer($parametre) {
    $idRequete = supprimerClient($parametre);
    $idRequete = listerClients();
    require_once 'mod_client/vue/clientVueListe.php';
}
