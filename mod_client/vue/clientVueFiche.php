<?php

$tpl = new Smarty();
switch ($parametre['action']) {
    case 'form_consulter':
        $row = $idRequete->fetch();
        $tpl->assign('code_c', $row['code_c']);
        $tpl->assign('nom', $row['nom']);
        $tpl->assign('adresse', $row['adresse']);
        $tpl->assign('cp', $row['cp']);
        $tpl->assign('ville', $row['ville']);
        $tpl->assign('telephone', $row['telephone']);
        break;
    case 'form_modifier':
        $row = $idRequete->fetch();
        $tpl->assign('code_c', $row['code_c']);
        $tpl->assign('nom', $row['nom']);
        $tpl->assign('adresse', $row['adresse']);
        $tpl->assign('cp', $row['cp']);
        $tpl->assign('ville', $row['ville']);
        $tpl->assign('telephone', $row['telephone']);
        $parametre['action'] = 'modifier';
        break;
    case'form_ajouter':
        $tpl->assign('code_c');
        $tpl->assign('nom');
        $tpl->assign('adresse');
        $tpl->assign('cp');
        $tpl->assign('ville');
        $tpl->assign('telephone');
        $parametre['action'] = 'ajouter';
        break;
    case'ajouter':
        $row = $idRequete->fetch();
        $tpl->assign('code_c', $row['code_c']);
        $tpl->assign('nom', $row['nom']);
        $tpl->assign('adresse', $row['adresse']);
        $tpl->assign('cp', $row['cp']);
        $tpl->assign('ville', $row['ville']);
        $tpl->assign('telephone', $row['telephone']);
        break;
    case 'form_supprimer':
        $row = $idRequete->fetch();
        $tpl->assign('code_c', $row['code_c']);
        $tpl->assign('nom', $row['nom']);
        $tpl->assign('adresse', $row['adresse']);
        $tpl->assign('cp', $row['cp']);
        $tpl->assign('ville', $row['ville']);
        $tpl->assign('telephone', $row['telephone']);
        $parametre['action'] = 'supprimer';
        break;
}
$tpl->assign('action', $parametre['action']);
$tpl->assign('msg', $msg);
$tpl->display('mod_client/vue/clientVueFiche.tpl');

