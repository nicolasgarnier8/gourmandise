

<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <body>

        {include file ='include/leftPanel.tpl'}
        {include file='include/rightPanel.tpl'}
        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>La gourmandise, ça se partage !</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="index.php">Accueil</a></li>
                            <li><a href="index.php?gestion=client">Clients</a></li>
                            <li class="active">Liste des clients</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content mt-3">
            <div class="animated fadeIn">

                <div class="row">

                    <div class="col-md-12">

                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title">Liste des clients

                                    <form class="pos-ajout" method="POST" action="index.php">
                                        <input type="hidden" name="gestion" value="client">
                                        <input type="hidden" name="action" value="form_ajouter">
                                        <label>Ajouter un Client : <input id="aImage" type="image" name="btn_ajouter" src='template/images/icones/a16.png'></label>
                                    </form>
                                </strong>
                            </div>
                            <div class="card-body">
                                <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Code Client</th>
                                            <th>Nom et Prénom</th>
                                            <th>Ville</th>
                                            <th>Téléphone</th>
                                            <th class="pos-actions">Consulter</th>
                                            <th class="pos-actions">Modifier</th>
                                            <th class="pos-actions">Supprimer</th>
                                            <th class="pos-actions">Commander</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        {foreach from =$listeClients item =client}
                                            <tr>
                                                <td>{$client.code_c}</td>
                                                <td>{$client.nom}</td>
                                                <td>{$client.ville}</td>
                                                <td>{$client.telephone}</td>
                                                <td class="pos-actions">
                                                    <form method="POST" action="index.php">
                                                        <input type="hidden" name="gestion" value="client">
                                                        <input type="hidden" name="action" value="form_consulter">
                                                        <input type="hidden" name="f_code_c" value="{$client.code_c}">
                                                        <input id="pImage" type="image" name="btn_consulter" src='template/images/icones/p16.png'>
                                                    </form
                                                </td>
                                                <td class="pos-actions">
                                                    <form method="POST" action="index.php">
                                                        <input type="hidden" name="gestion" value="client">
                                                        <input type="hidden" name="action" value="form_modifier">
                                                        <input type="hidden" name="f_code_c" value="{$client.code_c}">
                                                        <input id="mImage" type="image" name="btn_modifier" src='template/images/icones/m16.png'>
                                                    </form>
                                                </td>
                                                <td class="pos-actions">
                                                    <form method="POST" action="index.php">
                                                        <input type="hidden" name="gestion" value="client">
                                                        <input type="hidden" name="action" value="form_supprimer">
                                                        <input type="hidden" name="f_code_c" value="{$client.code_c}">
                                                        <input id="sImage" type="image" name="btn_supprimer" src='template/images/icones/s16.png'>
                                                    </form>
                                                </td>
                                                <td class="pos-actions">
                                                    <form method="POST" action="index.php">
                                                        <input type="hidden" name="gestion" value="commande">
                                                        <input type="hidden" name="action" value="form_ajouter">
                                                        <input type="hidden" name="f_code_c" value="{$client.code_c}">
                                                        <input id="sImage" type="image" name="btn_commander" src='template/images/icones/cmdvert16.png'>
                                                    </form>
                                                </td>

                                            </tr>
                                        {/foreach}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>


                </div><!-- .animated -->
            </div><!-- .content -->


        </div><!-- /#right-panel -->

        <!-- Right Panel -->
        <script src="template/assets/js/vendor/jquery-2.1.4.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"></script>
        <script src="template/assets/js/plugins.js"></script>
        <script src="template/assets/js/main.js"></script>


        <script src="template/assets/js/lib/data-table/datatables.min.js"></script>
        <script src="template/assets/js/lib/data-table/dataTables.bootstrap.min.js"></script>
        <script src="template/assets/js/lib/data-table/dataTables.buttons.min.js"></script>
        <script src="template/assets/js/lib/data-table/buttons.bootstrap.min.js"></script>
        <script src="template/assets/js/lib/data-table/jszip.min.js"></script>
        <script src="template/assets/js/lib/data-table/pdfmake.min.js"></script>
        <script src="template/assets/js/lib/data-table/vfs_fonts.js"></script>
        <script src="template/assets/js/lib/data-table/buttons.html5.min.js"></script>
        <script src="template/assets/js/lib/data-table/buttons.print.min.js"></script>
        <script src="template/assets/js/lib/data-table/buttons.colVis.min.js"></script>
        <script src="template/assets/js/lib/data-table/datatables-init.js"></script>


        <script type="text/javascript">
            $(document).ready(function () {
                $('#bootstrap-data-table-export').DataTable();
            });
        </script>

    </body>
</html>
