

<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->

    <body>
        {include file ='include/leftPanel.tpl'}
        {include file='include/rightPanel.tpl'}
        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>La gourmandise, ça se partage !</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="index.php">Accueil</a></li>
                            <li><a href="index.php?gestion=client">Clients</a></li>
                            <li class="active">{$msg}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content mt-3">
            <div class="animated fadeIn">

                <div class="row">

                    <div class="col-md-6">

                        <div class="card">
                            <div class="card-header"><strong>{$msg}</strong></div>
                            <form action="index.php" method="POST">
                                <input type="hidden" name="gestion"  value="client" >
                                <input type="hidden" name="action"  value={$action} >
                                <div class="card-body card-block">
                                    <input type="hidden" name="f_code_c"  class="form-control" readonly="readonly" value={$code_c}>
                                    <div class="form-group"><label for="nom" class=" form-control-label">Nom et Prénom : </label><input type="text" name="f_nom" class="form-control" value="{$nom}"></div>
                                    <div class="form-group"><label for="adresse" class=" form-control-label">Adresse : </label><input type="text" name="f_adresse" class="form-control" value="{$adresse}"></div>
                                    <div class="form-group"><label for="cp" class=" form-control-label">Code Postal : </label><input type="text" name="f_cp" class="form-control" value="{$cp}"></div>
                                    <div class="form-group"><label for="ville" class=" form-control-label">Ville : </label><input type="text" name="f_ville" class="form-control" value="{$ville}"></div>
                                    <div class="form-group"><label for="telephone" class=" form-control-label">Téléphone : </label><input type="text" name="f_telephone" class="form-control" value="{$telephone}"></div>

                                </div>
                                <div class="card-body card-block">
                                    <div class="col-md-6"> <input type='button' class="btn btn-submit" value='Retour' onclick='location.href = "index.php?gestion=client"'></div>

                                    {if $action eq 'ajouter'} <div class="col-md-6 "> <input type="submit" id="f_btn-action" class="btn btn-submit pos-btn-action" value="Ajouter" ></div>{/if}
                                    {if $action eq 'modifier'} <div class="col-md-6 "> <input type="submit" id="f_btn-action" class="btn btn-submit pos-btn-action" value="Modifier" ></div>{/if}
                                    {if $action eq 'supprimer'} <div class="col-md-6 "> <input type="submit" id="f_btn-action" class="btn btn-submit pos-btn-action" value="Supprimer" ></div>{/if}
                                    <br>
                                </div>
                            </form>
                        </div>
                    </div>
                </div><!-- .animated -->
            </div><!-- .content -->


        </div><!-- /#right-panel -->

        <!-- Right Panel -->
        <script src="template/assets/js/vendor/jquery-2.1.4.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"></script>
        <script src="template/assets/js/plugins.js"></script>
        <script src="template/assets/js/main.js"></script>


        <script src="template/assets/js/lib/data-table/datatables.min.js"></script>
        <script src="template/assets/js/lib/data-table/dataTables.bootstrap.min.js"></script>
        <script src="template/assets/js/lib/data-table/dataTables.buttons.min.js"></script>
        <script src="template/assets/js/lib/data-table/buttons.bootstrap.min.js"></script>
        <script src="template/assets/js/lib/data-table/jszip.min.js"></script>
        <script src="template/assets/js/lib/data-table/pdfmake.min.js"></script>
        <script src="template/assets/js/lib/data-table/vfs_fonts.js"></script>
        <script src="template/assets/js/lib/data-table/buttons.html5.min.js"></script>
        <script src="template/assets/js/lib/data-table/buttons.print.min.js"></script>
        <script src="template/assets/js/lib/data-table/buttons.colVis.min.js"></script>
        <script src="template/assets/js/lib/data-table/datatables-init.js"></script>


        <script type="text/javascript">
                                        $(document).ready(function () {
                                            $('#bootstrap-data-table-export').DataTable();
                                        });
        </script>

    </body>
</html>
