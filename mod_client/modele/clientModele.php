<?php

function listerClients() {
    $cnx = getBd();
    $sql = 'SELECT * FROM client';
    $idRequete = executeRequete($cnx, $sql);
    return $idRequete;
}

function ajouterClient($parametre) {
    $nom = $parametre['f_nom'];
    $adresse = $parametre['f_adresse'];
    $cp = $parametre['f_cp'];
    $ville = $parametre['f_ville'];
    $telephone = $parametre['f_telephone'];
    $cnx = getBd();
    $sql = "INSERT INTO client (nom, adresse, cp, ville, telephone) VALUES (?,?,?,?,?)";
    $idRequete = executeRequete($cnx, $sql, array($nom, $adresse, $cp, $ville, $telephone));
    return $idRequete;
}

function consulterClient($parametre) {
    $id = $parametre['f_code_c'];
    $cnx = getBd();
    $sql = "SELECT * FROM client WHERE code_c =?";
    $idRequete = executeRequete($cnx, $sql, array($id));
    return $idRequete;
}

function modifierClient($parametre) {
    $id = $parametre['f_code_c'];
    $nom = $parametre['f_nom'];
    $adresse = $parametre['f_adresse'];
    $cp = $parametre['f_cp'];
    $ville = $parametre['f_ville'];
    $telephone = $parametre['f_telephone'];
    $cnx = getBd();
    $sql = "UPDATE client SET nom=?,adresse=?,cp=?,ville=?,telephone=? WHERE code_c=?";
    $idRequete = executeRequete($cnx, $sql, array($nom, $adresse, $cp, $ville, $telephone, $id));
    return $idRequete;
}

function supprimerClient($parametre) {
    $id = $parametre['f_code_c'];
    $cnx = getBd();
    $sql = "DELETE FROM client WHERE code_c =?";
    $idRequete = executeRequete($cnx, $sql, array($id));
    return $idRequete;
}
