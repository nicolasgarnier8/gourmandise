<?php

function listerProduits() {
    $cnx = getBd();
    $sql = 'SELECT * FROM produit';
    $idRequete = executeRequete($cnx, $sql);
    return $idRequete;
}

function ajouterProduit($parametre) {
    $designation = $parametre['f_designation'];
    $quantite = $parametre['f_quantite'];
    $descriptif = $parametre['f_descriptif'];
    $prixUnitaireHT = $parametre['f_prix_unitaire_HT'];
    $stock = $parametre['f_stock'];
    $poidsPiece = $parametre['f_poids_piece'];
    $cnx = getBd();
    $sql = "INSERT INTO produit(designation,quantite,descriptif,prix_unitaire_HT,stock,poids_piece) VALUES (?,?,?,?,?,?)";
    $idRequete = executeRequete($cnx, $sql, array($designation, $quantite, $descriptif, $prixUnitaireHT, $stock, $poidsPiece));
    return $idRequete;
}

function consulterProduit($parametre) {
    $id = $parametre['f_reference'];
    $cnx = getBd();
    $sql = "SELECT * FROM produit WHERE reference=?";
    $idRequete = executeRequete($cnx, $sql, array($id));
    return $idRequete;
}

function modifierProduit($parametre) {
    $id = $parametre['f_reference'];
    $designation = $parametre['f_designation'];
    $quantite = $parametre['f_quantite'];
    $descriptif = $parametre['f_descriptif'];
    $prixUnitaireHT = $parametre['f_prix_unitaire_HT'];
    $stock = $parametre['f_stock'];
    $poidsPiece = $parametre['f_poids_piece'];
    $cnx = getBd();
    $sql = "UPDATE produit SET designation=?,quantite=?,descriptif=?,prix_unitaire_HT=?,stock=?,poids_piece=? WHERE reference=?";
    $idRequete = executeRequete($cnx, $sql, array($designation, $quantite, $descriptif, $prixUnitaireHT, $stock, $poidsPiece, $id));
    return $idRequete;
}

function supprimerProduit($parametre) {
    $id = $parametre['f_reference'];
    $cnx = getBd();
    $sql = "DELETE FROM produit WHERE reference=?";
    $idRequete = executeRequete($cnx, $sql, array($id));
    return $idRequete;
}
