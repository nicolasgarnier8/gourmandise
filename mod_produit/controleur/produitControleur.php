<?php

require_once 'mod_produit/modele/produitModele.php';

function vueParDefaut($parametre) {
    $idRequete = listerProduits($parametre);
    require_once 'mod_produit/vue/produitVueListe.php';
}

function ajouter($parametre) {
    $msg = "Fiche Produit : Création";
    require_once 'mod_produit/vue/produitVueFiche.php';
}

function valAjouter($parametre) {
    $idRequete = ajouterProduit($parametre);
    $idRequete = listerProduits($parametre);
    require_once 'mod_produit/vue/produitVueListe.php';
}

function consulter($parametre) {
    $msg = "Fiche Produit : Consultation";
    $idRequete = consulterProduit($parametre);
    require_once 'mod_produit/vue/produitVueFiche.php';
}

function modifier($parametre) {
    $msg = "Fiche Produit : Modification";
    $idRequete = consulterProduit($parametre);
    require_once 'mod_produit/vue/produitVueFiche.php';
}

function valModifier($parametre) {
    $idRequete = modifierProduit($parametre);
    $idRequete = consulterProduit($parametre);
    require_once 'mod_produit/vue/produitVueListe.php';
}

function supprimer($parametre) {
    $msg = "Fiche Produit : Suppresion";
    $idRequete = consulterProduit($parametre);
    require_once 'mod_produit/vue/produitVueFiche.php';
}

function valSupprimer($parametre) {
    $idRequete = supprimerProduit($parametre);
    $idRequete = listerProduits($parametre);
    require_once 'mod_produit/vue/produitVueListe.php';
}
