

<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
    <body>
        {include file ='include/leftPanel.tpl'}
        {include file='include/rightPanel.tpl'}
        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>La gourmandise, ça se partage !</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="index.php">Accueil</a></li>
                            <li><a href="index.php?gestion=produit">Produits</a></li>
                            <li class="active">{$msg}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content mt-3">
            <div class="animated fadeIn">

                <div class="row">

                    <div class="col-md-6">

                        <div class="card">
                            <div class="card-header"><strong>{$msg}</strong></div>
                            <form action="index.php" method="POST">
                                <input type="hidden" name="gestion"  value="produit" >
                                <input type="hidden" name="action"  value={$action} >
                                <div class="card-body card-block">
                                    <input type="hidden" name="f_reference"  class="form-control" readonly="readonly" value={$reference}>
                                    <div class="form-group"><label for="designation" class=" form-control-label">Désignation : </label><input type="text" name="f_designation" class="form-control" value="{$designation}"></div>
                                    <div class="form-group"><label for="quantite" class=" form-control-label">Quantité :<br><em> (Poids du produit ou nombre de pièces)</em></label><input type="text" name="f_quantite" class="form-control" value="{$quantite}"></div>
                                    <div class="form-group"><label for="descriptif" class=" form-control-label">Descriptif :<br><em>  Unité de mesure G pour gramme et P pour Pièce </em></label><input type="text" name="f_descriptif" class="form-control" value="{$descriptif}"></div>
                                    <div class="form-group"><label for="prix_unitaire_HT" class=" form-control-label">Prix unitaire hors taxes  : </label><input type="text" name="f_prix_unitaire_HT" class="form-control" value="{$prix_unitaire_HT}"></div>
                                    <div class="form-group"><label for="stock" class=" form-control-label">Etat du stock  : </label><input type="text" name="f_stock" class="form-control" value="{$stock}"></div>
                                    <div class="form-group"><label for="poids_piece" class=" form-control-label">Poids d'une pièce : <br><em> (en grammes pour les articles vendus par pièce )</em></label><input type="text" name="f_poids_piece" class="form-control" value="{$poids_piece}" ></div>

                                </div>
                                <div class="card-body card-block">
                                    <div class="col-md-6"> <input type='button' class="btn btn-submit" value='Retour' onclick='location.href = "index.php?gestion=produit"'></div>
                                    {if $action eq 'ajouter'} <div class="col-md-6 "> <input type="submit" id="f_btn-action" class="btn btn-submit pos-btn-action" value="Ajouter" ></div>{/if}
                                    {if $action eq 'modifier'} <div class="col-md-6 "> <input type="submit" id="f_btn-action" class="btn btn-submit pos-btn-action" value="Modifier" ></div>{/if}
                                    {if $action eq 'supprimer'} <div class="col-md-6 "> <input type="submit" id="f_btn-action" class="btn btn-submit pos-btn-action" value="Supprimer" ></div>{/if}
                                    <br>
                                </div>
                            </form>
                        </div>
                    </div>
                </div><!-- .animated -->
            </div><!-- .content -->


        </div><!-- /#right-panel -->

        <!-- Right Panel -->
        <script src="template/assets/js/vendor/jquery-2.1.4.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"></script>
        <script src="template/assets/js/plugins.js"></script>
        <script src="template/assets/js/main.js"></script>


        <script src="template/assets/js/lib/data-table/datatables.min.js"></script>
        <script src="template/assets/js/lib/data-table/dataTables.bootstrap.min.js"></script>
        <script src="template/assets/js/lib/data-table/dataTables.buttons.min.js"></script>
        <script src="template/assets/js/lib/data-table/buttons.bootstrap.min.js"></script>
        <script src="template/assets/js/lib/data-table/jszip.min.js"></script>
        <script src="template/assets/js/lib/data-table/pdfmake.min.js"></script>
        <script src="template/assets/js/lib/data-table/vfs_fonts.js"></script>
        <script src="template/assets/js/lib/data-table/buttons.html5.min.js"></script>
        <script src="template/assets/js/lib/data-table/buttons.print.min.js"></script>
        <script src="template/assets/js/lib/data-table/buttons.colVis.min.js"></script>
        <script src="template/assets/js/lib/data-table/datatables-init.js"></script>


        <script type="text/javascript">
                                        $(document).ready(function () {
                                            $('#bootstrap-data-table-export').DataTable();
                                        });
        </script>

    </body>
</html>
