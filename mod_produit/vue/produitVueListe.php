<?php

$tpl = new Smarty();
$listerProduits = array();
$i = 0;
while ($row = $idRequete->fetch()) {
    $listerProduits[$i]['reference'] = $row['reference'];
    $listerProduits[$i]['designation'] = $row['designation'];
    $listerProduits[$i]['prix_unitaire_HT'] = $row['prix_unitaire_HT'];
    $listerProduits[$i]['stock'] = $row['stock'];
    $i++;
}

$tpl->assign('listerProduits', $listerProduits);
$tpl->display('mod_produit/vue/produitVueListe.tpl');
