<?php

$tpl = new Smarty();
switch ($parametre['action']) {
    case 'form_consulter':
        $row = $idRequete->fetch();
        $tpl->assign('reference', $row['reference']);
        $tpl->assign('designation', $row['designation']);
        $tpl->assign('quantite', $row['quantite']);
        $tpl->assign('descriptif', $row['descriptif']);
        $tpl->assign('prix_unitaire_HT', $row['prix_unitaire_HT']);
        $tpl->assign('stock', $row['stock']);
        $tpl->assign('poids_piece', $row['poids_piece']);
        break;
    case'form_modifier':
        $row = $idRequete->fetch();
        $tpl->assign('reference', $row['reference']);
        $tpl->assign('designation', $row['designation']);
        $tpl->assign('quantite', $row['quantite']);
        $tpl->assign('descriptif', $row['descriptif']);
        $tpl->assign('prix_unitaire_HT', $row['prix_unitaire_HT']);
        $tpl->assign('stock', $row['stock']);
        $tpl->assign('poids_piece', $row['poids_piece']);
        $parametre['action'] = 'modifier';
        break;
    case'form_ajouter':
        $tpl->assign('reference');
        $tpl->assign('designation');
        $tpl->assign('quantite');
        $tpl->assign('descriptif');
        $tpl->assign('prix_unitaire_HT');
        $tpl->assign('stock');
        $tpl->assign('poids_piece');
        $parametre['action'] = 'ajouter';
        break;
    case'ajouter':
        $row = $idRequete->fetch();
        $tpl->assign('reference', $row['reference']);
        $tpl->assign('designation', $row['designation']);
        $tpl->assign('quantite', $row['quantite']);
        $tpl->assign('descriptif', $row['descriptif']);
        $tpl->assign('prix_unitaire_HT', $row['prix_unitaire_HT']);
        $tpl->assign('stock', $row['stock']);
        $tpl->assign('poids_piece', $row['poids_piece']);
        break;
    case'form_supprimer':
        $row = $idRequete->fetch();
        $tpl->assign('reference');
        $tpl->assign('designation');
        $tpl->assign('quantite');
        $tpl->assign('descriptif');
        $tpl->assign('prix_unitaire_HT');
        $tpl->assign('stock');
        $tpl->assign('poids_piece');
        $parametre['action'] = 'supprimer';
        break;
}
$tpl->assign('action', $parametre['action']);
$tpl->assign('msg', $msg);
$tpl->display('mod_produit/vue/produitVueFiche.tpl');
