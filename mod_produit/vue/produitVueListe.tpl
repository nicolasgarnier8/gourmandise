<!DOCTYPE html>

<html>
    <head>
        <title></title>
    </head>
    <body>
        {include file ='include/leftPanel.tpl'}
        {include file='include/rightPanel.tpl'}
        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>La gourmandise, ça se partage !</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="index.php">Accueil</a></li>
                            <li><a href="index.php?gestion=produit">Produits</a></li>
                            <li class="active">Liste des produits</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content mt-3">
            <div class="animated fadeIn">

                <div class="row">

                    <div class="col-md-12">

                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title">Liste des produits

                                    <form class="pos-ajout" method="POST" action="index.php">
                                        <input type="hidden" name="gestion" value="produit">
                                        <input type="hidden" name="action" value="form_ajouter">
                                        <label>Ajouter un Produit : <input id="aImage" type="image" name="btn_ajouter" src='template/images/icones/a16.png'></label>
                                    </form>
                                </strong>
                            </div>
                            <div class="card-body">
                                <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Référence</th>
                                            <th>Désignation</th>
                                            <th>Tarif HT</th>
                                            <th>Sock</th>
                                            <th class="pos-actions">Consulter</th>
                                            <th class="pos-actions">Modifier</th>
                                            <th class="pos-actions">Supprimer</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        {foreach from =$listerProduits item =produit}
                                            <tr>
                                                <td>{$produit.reference}</td>
                                                <td>{$produit.designation}</td>
                                                <td>{$produit.prix_unitaire_HT}</td>
                                                <td>{$produit.stock}</td>
                                                <td class="pos-actions">
                                                    <form method="POST" action="index.php">
                                                        <input type="hidden" name="gestion" value="produit">
                                                        <input type="hidden" name="action" value="form_consulter">
                                                        <input type="hidden" name="f_reference" value="{$produit.reference}">
                                                        <input id="pImage" type="image" name="btn_consulter" src='template/images/icones/p16.png'>
                                                    </form
                                                </td>
                                                <td class="pos-actions">
                                                    <form method="POST" action="index.php">
                                                        <input type="hidden" name="gestion" value="produit">
                                                        <input type="hidden" name="action" value="form_modifier">
                                                        <input type="hidden" name="f_reference" value="{$produit.reference}">
                                                        <input id="mImage" type="image" name="btn_modifier" src='template/images/icones/m16.png'>
                                                    </form>
                                                </td>
                                                <td class="pos-actions">
                                                    <form method="POST" action="index.php">
                                                        <input type="hidden" name="gestion" value="produit">
                                                        <input type="hidden" name="action" value="form_supprimer">
                                                        <input type="hidden" name="f_reference" value="{$produit.reference}">
                                                        <input id="sImage" type="image" name="btn_supprimer" src='template/images/icones/s16.png'>
                                                    </form>
                                                </td>
                                            </tr>
                                        {/foreach}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
