

<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
    <body>
        {include file ='include/leftPanel.tpl'}
        {include file='include/rightPanel.tpl'}
        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>La gourmandise, ça se partage !</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="index.php">Accueil</a></li>
                            <li><a href="index.php?gestion=commande">Commandes</a></li>
                            <li class="active">Fiche Commande : Consultation</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content mt-3">
            <div class="animated fadeIn">

                <div class="row">

                    <form action="index.php" method="POST">
                        <input type="hidden" name="gestion"  value="commande" >
                        <input type="hidden" name="action"  value="" >
                        <div class="col-md-12">

                            <div class="col-md-6">
                                <div class="card">
                                    <div class="card-header"><strong>Fiche Commande : Consultation</strong></div>

                                    <div class="card-body card-block" >

                                        <div class="form-group"name="f_numero">Numero : {$numero}</div>
                                        <div class="form-group">Vendeur : {$nom} {$prenom}</div>
                                        <div class="form-group">Code Client : {$code_c} </div>
                                        <div class="form-group">Client : {$nom_c} </div>


                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">

                                <div class="card">
                                    <div class="card-header"><strong>Etat de la commande</strong></div>
                                    <div class="card-body card-block">

                                        <div class="form-group">Date de commande : {$date_commande}</div>
                                        <div class="form-group">Date de livraison : {$date_livraison}</div>
                                        <div class="form-group">Total HT : {$total_ht} € </div>
                                        <div class="form-group">Commande Validée : {$etat} </div>


                                    </div>
                                </div>
                            </div>



                        </div>

                        <div class="col-md-12">
                            <!-- Liste lignes de commande -->

                            <div class="card-body">
                                <table  class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>N° Ligne</th>
                                            <th>Référence</th>
                                            <th>Désignation</th>
                                            <th>Quantité</th>
                                            <th>prix</th>
                                            <!--<th class="pos-actions">Consulter</th>
                                            <th class="pos-actions">Modifier</th>
                                            <th class="pos-actions">Supprimer</th>-->

                                        </tr>
                                    </thead>
                                    <tbody>
                                        {foreach from =$listeCommande item =ligne_commande}
                                            <tr>
                                                <td>{$ligne_commande.numero_ligne}</td>
                                                <td>{$ligne_commande.reference}</td>
                                                <td>{$ligne_commande.designation}</td>
                                                <td>{$ligne_commande.quantite_demandee}</td>
                                                <td>{$ligne_commande.prix_unitaire_HT*1.357}</td>

                                            </tr>
                                        {/foreach}
                                        <tr>
                                            <td colspan="3"> Montant de la commande : 226.62 € </td>
                                            <td colspan="2"> Total TVA : 45.32 €</td>
                                        </tr>

                                    </tbody>
                                </table>


                            </div>
                            <div class="card-body card-block">
                                <div class="col-md-6"> <input type='button' class="btn btn-submit" value='Retour' onclick='location.href = "index.php?gestion=commande"'></div>
                                <div class="col-md-6 "> </div>
                                <br>
                            </div>

                        </div>
                    </form>

                </div><!-- .content -->


            </div><!-- /#right-panel -->

            <!-- Right Panel -->
            <script src="template/assets/js/vendor/jquery-2.1.4.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"></script>
            <script src="template/assets/js/plugins.js"></script>
            <script src="template/assets/js/main.js"></script>


            <script src="template/assets/js/lib/data-table/datatables.min.js"></script>
            <script src="template/assets/js/lib/data-table/dataTables.bootstrap.min.js"></script>
            <script src="template/assets/js/lib/data-table/dataTables.buttons.min.js"></script>
            <script src="template/assets/js/lib/data-table/buttons.bootstrap.min.js"></script>
            <script src="template/assets/js/lib/data-table/jszip.min.js"></script>
            <script src="template/assets/js/lib/data-table/pdfmake.min.js"></script>
            <script src="template/assets/js/lib/data-table/vfs_fonts.js"></script>
            <script src="template/assets/js/lib/data-table/buttons.html5.min.js"></script>
            <script src="template/assets/js/lib/data-table/buttons.print.min.js"></script>
            <script src="template/assets/js/lib/data-table/buttons.colVis.min.js"></script>
            <script src="template/assets/js/lib/data-table/datatables-init.js"></script>


            <script type="text/javascript">
                                    $(document).ready(function () {
                                        $('#bootstrap-data-table-export').DataTable();
                                    });
            </script>

    </body>
</html>
