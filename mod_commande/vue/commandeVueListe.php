<?php

$tpl = new Smarty();

$listeCommande = array();
$i = 0;
while ($row = $idRequete->fetch()) {
    $listeCommande[$i]['numero'] = $row['numero'];
    $listeCommande[$i]['nom'] = $row['nom'];
    $listeCommande[$i]['prenom'] = $row['prenom'];
    $listeCommande[$i]['code_c'] = $row['code_c'];
    $listeCommande[$i]['nom_c'] = $row['nom_c'];
    $listeCommande[$i]['total_ht'] = $row['total_ht'];
    $i++;
}
$nbLigne = $idRequete->rowCount();
$tpl->assign('nbLigne', $nbLigne);
$tpl->assign('listeCommande', $listeCommande);
$tpl->display('mod_commande/vue/commandeVueListe.tpl');
