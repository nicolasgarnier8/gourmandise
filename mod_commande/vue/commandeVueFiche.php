<?php

$tpl = new Smarty();
switch ($parametre['action']) {
    case 'form_consulter':
        $row = $idRequete->fetch();
        $tpl->assign('numero', $row['numero']);
        $tpl->assign('nom', $row['nom']);
        $tpl->assign('prenom', $row['prenom']);
        $tpl->assign('code_c', $row['code_c']);
        $tpl->assign('nom_c', $row['nom_c']);
        $tpl->assign('date_commande', $row['date_commande']);
        $tpl->assign('date_livraison', $row['date_livraison']);
        $tpl->assign('total_ht', $row['total_ht']);
        $tpl->assign('etat', $row['etat']);
        break;
}
$listeCommande = array();
$i = 0;
while ($row = $idRequetes->fetch()) {
    $listeCommande[$i]['numero_ligne'] = $row['numero_ligne'];
    $listeCommande[$i]['reference'] = $row['reference'];
    $listeCommande[$i]['designation'] = $row['designation'];
    $listeCommande[$i]['quantite_demandee'] = $row['quantite_demandee'];
    $listeCommande[$i]['prix_unitaire_HT'] = $row['prix_unitaire_HT'];
    $i++;
}
$tpl->assign('listeCommande', $listeCommande);
$tpl->assign('action', $parametre['action']);
$tpl->display('mod_commande/vue/commandeVueFiche.tpl');
