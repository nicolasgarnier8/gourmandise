<?php

require_once 'mod_commande/modele/commandeModele.php';

function vueParDefaut($parametre) {
    $idRequete = listerCommande($parametre);
    require_once 'mod_commande/vue/commandeVueListe.php';
}

function consulter($parametre) {
    $msg = "Fiche Commande : Consultation";
    $idRequete = consulterCommande($parametre);
    $idRequetes = listeArticle($parametre);
    require_once 'mod_commande/vue/commandeVueFiche.php';
}
