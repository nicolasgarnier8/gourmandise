<?php

function listerCommande($parametre) {
    $cnx = getBd();
    $sql = "SELECT * FROM commande, vendeur, client WHERE client.code_c=commande.code_c AND vendeur.code_v=commande.code_v ";
    $idRequete = executeRequete($cnx, $sql);
    return $idRequete;
}

function consulterCommande($parametre) {
    $numero = $parametre['f_numero'];
    $cnx = getBd();
    $sql = "SELECT commande.numero, vendeur.nom, vendeur.prenom, client.code_c, client.nom_c, commande.date_commande, commande.date_livraison, commande.total_ht, commande.etat "
            . "FROM client, commande, vendeur WHERE client.code_c=commande.code_c AND commande.code_v=vendeur.code_v AND commande.numero=? GROUP BY commande.numero";
    $idRequete = executeRequete($cnx, $sql, array($numero));
    return $idRequete;
}

function listeArticle($parametre) {
    $numero = $parametre['f_numero'];
    $cnx = getBd();
    $sql = "SELECT ligne_commande.numero_ligne, produit.reference, produit.designation, ligne_commande.quantite_demandee, produit.prix_unitaire_HT FROM commande, produit, ligne_commande WHERE produit.reference=ligne_commande.reference AND ligne_commande.numero=commande.numero AND commande.numero=?";
    $idRequetes = executeRequete($cnx, $sql, array($numero));
    return $idRequetes;
}
