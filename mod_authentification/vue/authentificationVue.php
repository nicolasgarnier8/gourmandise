<?php

$tpl = new Smarty();

if (isset($parametre['action'])) {
    $tpl->assign('erreur', 'Identification invalide');
} else {
    //premier passage
    $tpl->assign('erreur', '');
}
$tpl->assign('login', '');
$tpl->assign('pw', '');
$tpl->display('mod_authentification/vue/authentificationVue.tpl');
