<?php

require_once 'mod_authentification/modele/authentificationModele.php';

/**
 *
 * @param type $parametre
 */
function vueParDefaut($parametre) {
    require_once 'mod_authentification/vue/authentificationVue.php';
}

function authentifier($parametre) {
    $idRequete = authentificationExiste($parametre);
    $login = $parametre['login'];
    $pw = $parametre['pw'];
    $row = $idRequete->fetch(PDO::FETCH_NUM);
    $gauche = "ar30&y%";
    $droite = "tk!@";
    $jeton = hash('ripemd128', "$gauche$pw$droite");
    if (($idRequete->rowCount() == 1) && ($jeton == $row[8])) {
        session_start();
        $_SESSION['login'] = $login;
        $_SESSION['prenom'] = $row[2];
        $_SESSION['nom'] = $row[1];
        $_SESSION['code_v'] = $row[0];
        header('location:index.php');
    } else {
        require_once 'mod_authentification/vue/authentificationVue.php';
    }
}

//Fin connection